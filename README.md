﻿# RPG Test

## Setup

Open using the IDE of your choice and pull the dependencies using NuGet. To preprocess the assets,
you need to use the [MonoGame Content Builder](https://docs.monogame.net/articles/tools/mgcb.html).
To open the map files, use the [Tiled map editor](https://www.mapeditor.org/).

## Structure

The assets for this project are currently being taken from Opengameart, in particular the following
set is used https://opengameart.org/content/tiny-16-basic.

The `Content/` folder contains all the game's assets (before compilation) organized neatly into different
subfolders.

The source code is divided into the `Engine/` and `Impl/` folders. The former contains basic groundwork
for the game, whereas the second contains implementations of the actual content.

## What is currently supported?

Currently, the engine can handle the following:
* Rendering Tiled maps, supporting a collision layer and a customizable player spawn point
* Rendering sprites with animations
* Sprite-to-sprite and sprite-to-map collision detection
* Map size aware camera

The next steps likely include:
* Particles
* HUD
* Dialog
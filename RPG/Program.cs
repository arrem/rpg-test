﻿using System;
using RPG.Impl;

namespace RPG
{
    internal class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            using var game = new RPGGame(1280, 720);
            game.Run();
        }
    }
}
﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RPG.Engine.Sprites;

namespace RPG.Impl.Sprites
{
    public sealed class SplashScreenSprite : Sprite
    {
        private const float OpacityFadingRate = 0.3f;
        private float _opacity = 1f;

        private bool _shouldFade;

        public EventHandler<EventArgs> FadedOut;

        public SplashScreenSprite(int viewportWidth, int viewportHeight, Texture2D texture) : base(texture)
        {
            Position = Vector2.Zero;
            Width = viewportWidth;
            Height = viewportHeight;
        }

        public override int Width { get; }
        public override int Height { get; }

        public void FadeOut()
        {
            _shouldFade = true;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (!_shouldFade) { return; }

            _opacity -= OpacityFadingRate;

            if (_opacity <= 0) { FadedOut?.Invoke(this, EventArgs.Empty); }
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            var destinationRectangle = new Rectangle((int) Position.X, (int) Position.Y, Width, Height);
            spriteBatch.Draw(Texture, destinationRectangle, Color.White * _opacity);
        }
    }
}
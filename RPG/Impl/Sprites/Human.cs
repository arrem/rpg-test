﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using RPG.Engine.Assets;
using RPG.Engine.Sprites;

namespace RPG.Impl.Sprites
{
    public sealed class Human : CollidableSprite
    {
        private const string TexturePath = "Graphics/Characters/protag";
        private const float Speed = 2f;
        private readonly Dictionary<string, Animation> _animations;
        private Animation _currentAnimation;

        private Direction _direction = Direction.Down;

        public Human(AssetManager manager) : base(manager.GetTexture(TexturePath))
        {
            Position = Vector2.Zero;
            Bounds = new RectangleF(2f, 0f, Width - 4, Height);

            _animations = manager.GetAnimations("Graphics/Animations/protagWalk");
            _currentAnimation = _animations["IdleDown"];
        }

        public override int Width => 16;
        public override int Height => 32;

        protected override bool PreventClipThrough => true;

        public override Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                PositionChanged?.Invoke(this, value);
            }
        }

        public event EventHandler<Vector2> PositionChanged;

        public void Move(Direction direction)
        {
            var oldDirection = _direction;
            _direction = direction;

            Position += direction switch
            {
                Direction.Up => new Vector2(0, -Speed),
                Direction.Down => new Vector2(0, Speed),
                Direction.Left => new Vector2(-Speed, 0),
                Direction.Right => new Vector2(Speed, 0),
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };
            
            if (_currentAnimation.Category == "Idle")
            {
                _currentAnimation = _animations["Walk" + _currentAnimation.Name.Substring(4)];
            }

            if (_direction == oldDirection) { return; }

            _currentAnimation = direction switch
            {
                Direction.Left => _animations["WalkLeft"],
                Direction.Right => _animations["WalkRight"],
                Direction.Up => _animations["WalkUp"],
                Direction.Down => _animations["WalkDown"],
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };
            _currentAnimation.Reset();
        }

        public void StopMoving()
        {
            if (_currentAnimation.Category == "Walk")
            {
                _currentAnimation = _animations["Idle" + _currentAnimation.Name.Substring(4)];
            }
        }

        public override void Update(GameTime gameTime)
        {
            _currentAnimation.Update();
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            var sourceRectangle = _currentAnimation.CurrentFrame;
            var destinationRectangle = new Rectangle((int) Position.X, (int) Position.Y, Width, Height);

            spriteBatch.Draw(Texture, destinationRectangle, sourceRectangle, Color.White);
        }
    }
}
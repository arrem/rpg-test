﻿using Microsoft.Xna.Framework;
using RPG.Engine.Assets;
using RPG.Engine.Sprites;

namespace RPG.Impl.Sprites
{
    public class Torch : Sprite, ILightSource
    {
        private const string TexturePath = "Graphics/Objects/Torch";
        private const string LightTexturePath = "Graphics/Lights/circularLight";
        private const float LightRadius = 30f;
        private const float LightOpacity = 0.9f;
        private static readonly Color LightColor = new(230, 230, 100);

        public Torch(AssetManager manager) : base(manager.GetTexture(TexturePath))
        {
            Light = new Light(manager.GetTexture(LightTexturePath), LightRadius, LightColor, LightOpacity);
        }

        public override Vector2 Position
        {
            get => base.Position;
            set
            {
                base.Position = value;
                Light.Position = value + new Vector2(Width / 2f, Height / 2f);
            }
        }

        public Light Light { get; }
    }
}
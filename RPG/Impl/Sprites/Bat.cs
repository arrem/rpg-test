﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RPG.Engine.Assets;
using RPG.Engine.Sprites;

namespace RPG.Impl.Sprites
{
    public sealed class Bat : Sprite
    {
        private const string TexturePath = "Graphics/Characters/bat";
        private readonly Dictionary<string, Animation> _animations;

        private readonly Random _random = new();
        private Animation _currentAnimation;

        private Direction _direction = Direction.Down;
        private Vector2 _velocity = Vector2.Zero;

        public Bat(AssetManager manager) : base(manager.GetTexture(TexturePath))
        {
            Position = Vector2.Zero;

            _animations = manager.GetAnimations("Graphics/Animations/playerWalk");
            _currentAnimation = _animations["WalkDown"];
        }

        public override int Width => 16;
        public override int Height => 16;

        private void Move(Direction direction)
        {
            var oldDirection = _direction;
            _direction = direction;

            if (_direction == oldDirection) { return; }

            _currentAnimation = direction switch
            {
                Direction.Left => _animations["WalkLeft"],
                Direction.Right => _animations["WalkRight"],
                Direction.Up => _animations["WalkUp"],
                Direction.Down => _animations["WalkDown"],
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };
            _currentAnimation.Reset();
        }

        public override void Update(GameTime gameTime)
        {
            if (_random.Next(10) > 5)
            {
                _velocity = new Vector2(
                    Math.Clamp(_velocity.X + ((float) _random.NextDouble() - 0.5f) / 3f, -2f, 2f),
                    Math.Clamp(_velocity.Y + ((float) _random.NextDouble() - 0.5f) / 3f, -2f, 2f)
                );
            }

            Position += _velocity;

            if (_velocity.X >= _velocity.Y) { Move(_velocity.X >= 0 ? Direction.Right : Direction.Left); }
            else { Move(_velocity.Y >= 0 ? Direction.Down : Direction.Up); }

            _currentAnimation.Update();
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            var sourceRectangle = _currentAnimation.CurrentFrame;
            var destinationRectangle = new Rectangle((int) Position.X, (int) Position.Y, Width, Height);

            spriteBatch.Draw(Texture, destinationRectangle, sourceRectangle, Color.White);
        }
    }
}
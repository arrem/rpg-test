﻿using RPG.Engine;
using RPG.Impl.States;

namespace RPG.Impl
{
    public sealed class RPGGame : BaseGame
    {
        public RPGGame(int width, int height) : base(width, height, new SplashScreenState()) { }
    }
}
﻿using Microsoft.Xna.Framework;
using RPG.Engine.Assets;
using RPG.Engine.Interfaces;

namespace RPG.Impl.Interfaces
{
    public class CombatInterface : Interface
    {
        public CombatInterface(AssetManager manager)
        {
            AddComponent(new HealthBar(manager) {Position = new Vector2(10)});
        }
    }
}
﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using RPG.Engine.Assets;
using RPG.Engine.Interfaces;

namespace RPG.Impl.Interfaces
{
    public sealed class DialogBox : InterfaceComponent
    {
        private const string BoxTexturePath = "Graphics/Interface/dialogBox";
        private const string ArrowTexturePath = "Graphics/Interface/arrow";

        private const int PortraitPadding = 5;
        private const int BottomSpacing = 10;
        private const int ArrowSize = 40;
        private const int NameSize = 60;

        private const int NextCharacterDelay = 1;

        private readonly Texture2D _boxTexture;
        private readonly Texture2D _arrowTexture;
        private readonly Texture2D _portraitTexture;

        private readonly Vector2 _boxPosition;
        private readonly int _totalWidth;

        private readonly string[] _lines;
        private int _lineIndex = 0;
        private string _display = "";
        private int _life = 0;
        
        private readonly SpriteFont _nameFont;
        private readonly SpriteFont _dialogFont;
        private readonly string _character;

        private string CurrentLine => _lines[_lineIndex];
        private bool CurrentLineDone => _display.Length == CurrentLine.Length;
        private bool HasMoreLines => _lineIndex < _lines.Length - 1;

        public event EventHandler<EventArgs> DialogFinished; 
        
        public DialogBox(AssetManager manager, int viewportWidth, int viewportHeight, string character, string[] lines)
        {
            _nameFont = manager.GetFont("Fonts/IndieFlower20");
            _dialogFont = manager.GetFont("Fonts/IndieFlower15");
            
            _boxTexture = manager.GetTexture(BoxTexturePath);
            _arrowTexture = manager.GetTexture(ArrowTexturePath);
            _portraitTexture = manager.GetTexture($"Graphics/Portraits/{character}");

            _character = character;
            _lines = lines;

            _totalWidth = _portraitTexture.Width + _boxTexture.Width + PortraitPadding;
            
            Position = new Vector2(
                (viewportWidth - _totalWidth) / 2,
                viewportHeight - _boxTexture.Height - BottomSpacing
            );

            _boxPosition = Position + new Vector2(_portraitTexture.Width + PortraitPadding, 0);
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_portraitTexture, Position, Color.White);
            spriteBatch.Draw(_boxTexture, _boxPosition, Color.White);
            
            spriteBatch.DrawString(_nameFont, _character, _boxPosition + new Vector2(ArrowSize + 1, 1), Color.Black * 0.3f);
            spriteBatch.DrawString(_nameFont, _character, _boxPosition + new Vector2(ArrowSize, 0), Color.White);
            
            spriteBatch.DrawString(_dialogFont, _display, _boxPosition + new Vector2(ArrowSize + 1, NameSize + 1), Color.Black * 0.3f);
            spriteBatch.DrawString(_dialogFont, _display, _boxPosition + new Vector2(ArrowSize, NameSize), Color.White);

            if (HasMoreLines)
            {
                spriteBatch.Draw(_arrowTexture, new Vector2(Position.X + _totalWidth - _arrowTexture.Width - 10, Position.Y + _boxTexture.Height - _arrowTexture.Height - 10), Color.White);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (!CurrentLineDone)
            {
                if (_life >= NextCharacterDelay)
                {
                    _display += CurrentLine[_display.Length];
                    _life = 0;
                }

                _life++;
            }
        }

        public void HandleClick()
        {
            if (!CurrentLineDone)
            {
                _display = CurrentLine;
                return;
            }

            if (!HasMoreLines)
            {
                DialogFinished?.Invoke(this, EventArgs.Empty);
                return;
            }

            _lineIndex++;
            _display = "";
        }
    }
}
﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using RPG.Engine.Assets;
using RPG.Engine.Interfaces;
using RPG.Engine.Sprites;

namespace RPG.Impl.Interfaces
{
    public sealed class Minimap : InterfaceComponent
    {
        private readonly Map _map;
        private readonly float _worldScale;
        private readonly float _mapScale;

        private readonly float _tileWidth;
        private readonly float _tileHeight;

        private readonly float _mapWidth;
        private readonly float _mapHeight;

        private readonly List<Sprite> _sprites = new();

        public Minimap(Map map, float worldScale, float mapScale)
        {
            _map = map;
            _worldScale = worldScale;
            _mapScale = mapScale;

            _tileWidth = _map.TiledMap.TileWidth;
            _tileHeight = _map.TiledMap.TileHeight;

            _mapWidth = _map.TiledMap.Width;
            _mapHeight = _map.TiledMap.Height;
        }

        public void AddSprite(Sprite sprite)
        {
            _sprites.Add(sprite);
        }

        public void RemoveSprite(Sprite sprite)
        {
            _sprites.Remove(sprite);
        }

        public override void Render(SpriteBatch spriteBatch)
        {
            var (mx, my) = Position;
            
            spriteBatch.DrawRectangle(
                new RectangleF(mx - 2, my - 2, _mapWidth * _mapScale + 4, _mapHeight * _mapScale + 4), Color.Black, 2f);
            spriteBatch.Draw(_map.MinimapTexture,
                new Rectangle((int) mx, (int) my, (int) (_mapWidth * _mapScale), (int) (_mapHeight * _mapScale)),
                Color.White);

            foreach (var sprite in _sprites)
            {
                spriteBatch.DrawRectangle(
                    new RectangleF(mx + sprite.Position.X / _tileWidth * _worldScale,
                        my + sprite.Position.Y / _tileHeight * _worldScale, 3, 3), Color.Red, 3f);
            }
        }
    }
}
﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RPG.Engine.Assets;
using RPG.Engine.Interfaces;

namespace RPG.Impl.Interfaces
{
    public sealed class HealthBar : InterfaceComponent
    {
        private const int NamePadding = 20;

        private readonly Texture2D _background;
        private readonly Texture2D _healthBarEmptyTexture;
        private readonly Texture2D _healthBarFullTexture;


        private readonly SpriteFont _nameFont;

        public HealthBar(AssetManager manager)
        {
            Name = "Arrem";
            _nameFont = manager.GetFont("Fonts/IndieFlower20");
            _healthBarEmptyTexture = manager.GetTexture("Graphics/Interface/healthBarEmpty");
            _healthBarFullTexture = manager.GetTexture("Graphics/Interface/healthBarFull");
            _background = new Texture2D(_healthBarEmptyTexture.GraphicsDevice, 1, 1);
            _background.SetData(new[] {Color.White});
        }

        /// <summary>
        ///     Controls the name that this health bar should display.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Controls the percentage of health that the health bar should display.
        /// </summary>
        public float Health { get; set; } = 0.75f;

        public override void Render(SpriteBatch spriteBatch)
        {
            var (fontWidth, _) = _nameFont.MeasureString(Name);

            spriteBatch.Draw(_background,
                new Rectangle((int) Position.X, (int) Position.Y, (int) fontWidth + NamePadding,
                    _healthBarEmptyTexture.Height), Color.Black);
            spriteBatch.DrawString(_nameFont, Name, Position + new Vector2(4, 4), Color.White);

            var position = Position + new Vector2(fontWidth + NamePadding, 0);

            var sourceRectangle = new Rectangle(0, 0, (int) (_healthBarFullTexture.Width * Health),
                _healthBarFullTexture.Height);

            spriteBatch.Draw(_healthBarEmptyTexture, position, Color.White);
            spriteBatch.Draw(_healthBarFullTexture, position, sourceRectangle, Color.White);
        }
    }
}
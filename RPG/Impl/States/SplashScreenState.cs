﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using RPG.Engine.States;
using RPG.Impl.Sprites;

namespace RPG.Impl.States
{
    public sealed class SplashScreenState : GameState
    {
        private const string SplashScreenSpriteTexture = "Graphics/Screens/splash";

        private SplashScreenSprite _splashScreenSprite;

        protected override int Scale => 1;

        public override void LoadContent()
        {
            _splashScreenSprite = new SplashScreenSprite(ViewportWidth, ViewportHeight,
                AssetManager.GetTexture(SplashScreenSpriteTexture));
            _splashScreenSprite.FadedOut += (_, _) => SwitchState(new GameplayState());

            AddSprite(_splashScreenSprite);
        }

        public override void UnloadContent() { }

        public override void HandleInput(GameTime gameTime)
        {
            var state = Keyboard.GetState();

            if (state.GetPressedKeyCount() > 0) { _splashScreenSprite.FadeOut(); }
        }

        protected override void UpdateLogic(GameTime gameTime)
        {
            _splashScreenSprite.Update(gameTime);
        }
    }
}
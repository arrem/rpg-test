﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using RPG.Engine.Sprites;
using RPG.Engine.States;
using RPG.Impl.Interfaces;
using RPG.Impl.Sprites;

namespace RPG.Impl.States
{
    public sealed class GameplayState : GameState
    {
        private readonly List<Human> _friends = new();

        private Minimap _minimap;
        private CombatInterface _interface;
        private DialogBox _dialogBox;

        private bool _mouseDown;

        private Human _player;
        protected override int Scale => 2;

        public override void LoadContent()
        {
            SetMap("plain");

            _interface = new CombatInterface(AssetManager);

            var spawnPoint = Map.Points["Spawn"];

            _player = new Human(AssetManager)
            {
                Position = spawnPoint.Position
            };
            _player.PositionChanged += (_, position) => CameraLookAt(position);

            AddSprite(_player);

            const int length = 60;
            const double total = 2 * Math.PI;
            const int characters = 10;

            for (var i = 0d; i < total - 0.1d; i += total / characters)
            {
                var x = length * Math.Cos(i);
                var y = length * Math.Sin(i);

                var friend = new Human(AssetManager)
                {
                    Position = _player.Position + new Vector2((float) x, (float) y)
                };
                AddSprite(friend);
                _friends.Add(friend);
            }

            CameraLookAt(_player.Position);

            _minimap = new Minimap(Map, Scale, 2f) {Position = new Vector2(20)};
            _minimap.AddSprite(_player);

            _dialogBox = new DialogBox(AssetManager, ViewportWidth, ViewportHeight, "maple",
                new[]
                {
                    "hello my name is mambo and i like moth frogs\nthey're like regular frogs but eat cheese\nmy friend syro likes cheesy cheese on his cheesy cheese... xD",
                    "moths are kinda my thing", 
                    "i eat snails", 
                    "bababababaaaaaaaaa"
                });

            _dialogBox.DialogFinished += (_, _) => { _dialogBox = null; };
        }

        public override void UnloadContent() { }

        protected override void UpdateLogic(GameTime gameTime)
        {
            _player.Update(gameTime);
            _dialogBox?.Update(gameTime);
        }

        public override void HandleInput(GameTime gameTime)
        {
            var keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Keys.S)) { _player.Move(Direction.Down); }
            else if (keyboardState.IsKeyDown(Keys.A)) { _player.Move(Direction.Left); }
            else if (keyboardState.IsKeyDown(Keys.D)) { _player.Move(Direction.Right); }
            else if (keyboardState.IsKeyDown(Keys.W)) { _player.Move(Direction.Up); }
            else { _player.StopMoving(); }

            AmbientColor = keyboardState.IsKeyDown(Keys.LeftShift) ? new Color(65, 30, 65) : Color.White;

            var mouseState = Mouse.GetState();

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                _mouseDown = true;
                var (mouseX, mouseY) = Vector2.Transform(new Vector2(mouseState.X, mouseState.Y),
                    Camera.GetInverseViewMatrix());

                foreach (var friend in _friends.Where(friend =>
                    friend.Bounds.Intersects(new RectangleF(mouseX, mouseY, 2, 2)))) { friend.Destroyed = true; }
            }
            else
            {
                if (_mouseDown)
                {
                    /*var mouse = Vector2.Transform(new Vector2(mouseState.X, mouseState.Y),
                        Camera.GetInverseViewMatrix());
                    var torch = new Torch(AssetManager);
                    torch.Position = mouse - new Vector2(torch.Width / 2f, torch.Height / 2f);
                    AddSprite(torch);*/

                    _dialogBox?.HandleClick();
                }

                _mouseDown = false;
            }
        }

        public override void RenderInterface(SpriteBatch spriteBatch)
        {
            _minimap.Render(spriteBatch);
            _dialogBox?.Render(spriteBatch);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Collisions;
using MonoGame.Extended.ViewportAdapters;
using RPG.Engine.Assets;
using RPG.Engine.Sprites;

namespace RPG.Engine.States
{
    public abstract class GameState
    {
        private readonly List<ICollisionActor> _collisionActors = new();
        private readonly List<Light> _lights = new();
        private readonly List<Sprite> _sprites = new();

        private CollisionComponent _collisionComponent;
        private ContentManager _contentManager;
        private GraphicsDevice _graphicsDevice;

        protected AssetManager AssetManager;

        public OrthographicCamera Camera;

        protected Map Map;

        protected int ViewportHeight;
        protected int ViewportWidth;

        public Color AmbientColor { get; protected set; } = Color.White;

        protected abstract int Scale { get; }

        public event EventHandler<GameState> StateSwitched;

        public abstract void LoadContent();
        public abstract void UnloadContent();


        public void Initialize(ContentManager contentManager, GameWindow window, GraphicsDevice graphicsDevice,
            int viewportWidth, int viewportHeight)
        {
            _contentManager = contentManager;
            ViewportHeight = viewportHeight;
            ViewportWidth = viewportWidth;
            _graphicsDevice = graphicsDevice;

            var viewportAdapter = new BoxingViewportAdapter(window, graphicsDevice, viewportWidth, viewportHeight);
            Camera = new OrthographicCamera(viewportAdapter) {Zoom = Scale};

            AssetManager = new AssetManager(_contentManager);
        }

        protected void AddSprite(Sprite sprite)
        {
            _sprites.Add(sprite);

            if (sprite is ICollisionActor actor) { AddCollisionActor(actor); }

            if (sprite is ILightSource source) { AddLight(source.Light); }
        }

        protected void RemoveSprite(Sprite sprite)
        {
            _sprites.Remove(sprite);

            if (sprite is ICollisionActor actor) { RemoveCollisionActor(actor); }

            if (sprite is ILightSource source) { RemoveLight(source.Light); }
        }

        protected void AddLight(Light light)
        {
            _lights.Add(light);
        }

        protected void RemoveLight(Light light)
        {
            _lights.Remove(light);
        }

        protected void AddCollisionActor(ICollisionActor actor)
        {
            _collisionActors.Add(actor);
            _collisionComponent.Insert(actor);
        }

        protected void RemoveCollisionActor(ICollisionActor actor)
        {
            _collisionActors.Remove(actor);
            _collisionComponent.Remove(actor);
        }

        protected void SetMap(string mapName)
        {
            Map = AssetManager.GetMap($"Maps/{mapName}", _graphicsDevice);

            _collisionComponent =
                new CollisionComponent(new RectangleF(0f, 0f, Map.TiledMap.WidthInPixels, Map.TiledMap.HeightInPixels));
            _collisionActors.Clear();

            foreach (var actor in Map.CollisionActors) { AddCollisionActor(actor); }
        }

        protected void SwitchState(GameState gameState)
        {
            StateSwitched?.Invoke(this, gameState);
        }

        public abstract void HandleInput(GameTime gameTime);


        public void Update(GameTime gameTime)
        {
            Map?.Renderer.Update(gameTime);

            UpdateLogic(gameTime);

            _collisionComponent?.Update(gameTime);

            var toRemove = _sprites.FindAll(s => s.Destroyed);
            foreach (var sprite in toRemove)
            {
                _sprites.Remove(sprite);
                if (sprite is not ICollisionActor actor) { continue; }

                _collisionActors.Remove(actor);
                _collisionComponent?.Remove(actor);
            }
        }

        protected virtual void UpdateLogic(GameTime gameTime) { }

        public void Render(SpriteBatch spriteBatch)
        {
            Map?.Renderer.Draw(Camera.GetViewMatrix());

            foreach (var sprite in _sprites.OrderBy(s => s.Position.Y)) { sprite.Render(spriteBatch); }
        }

        public void RenderLights(SpriteBatch spriteBatch)
        {
            foreach (var light in _lights) { light.Render(spriteBatch); }
        }

        public virtual void RenderInterface(SpriteBatch spriteBatch) { }

        /// <summary>
        ///     Makes the camera try to look at the specified position, while keeping the map in bounds. Effectively,
        ///     if the map is smaller than the screen in one direction, the camera will never move in that direction and
        ///     will keep the map centered in it.
        ///     <para />
        ///     If the map is bigger than the screen in one direction, the camera will keep the player centered in that
        ///     direction for as long as they have map space left, but it will not let the camera show empty space around
        ///     the map.
        /// </summary>
        /// <param name="position">The position to try to center the camera on.</param>
        protected void CameraLookAt(Vector2 position)
        {
            var box = Camera.BoundingRectangle;
            var x = Map.Width <= box.Width
                ? Map.Width / 2f
                : Math.Clamp(position.X, box.Width / 2, Map.Width - box.Width / 2);
            var y = Map.Height <= box.Height
                ? Map.Height / 2f
                : Math.Clamp(position.Y, box.Height / 2, Map.Height - box.Height / 2);

            Camera.LookAt(new Vector2(x, y));
        }
    }
}
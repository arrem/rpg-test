﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RPG.Engine.States;

namespace RPG.Engine
{
    public abstract class BaseGame : Game
    {
        private readonly GameState _firstGameState;
        private readonly bool _fullscreen = false;
        protected readonly GraphicsDeviceManager Graphics;
        private Rectangle _baseRenderRectangle;
        private int _height;

        private int _width;

        protected GameState CurrentGameState;
        protected RenderTarget2D RenderTarget;
        protected RenderTarget2D RenderTargetLights;
        protected SpriteBatch SpriteBatch;

        protected BaseGame(int width, int height, GameState firstState)
        {
            _width = width;
            _height = height;
            _firstGameState = firstState;
            IsMouseVisible = true;

            Graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = _width,
                PreferredBackBufferHeight = _height
            };

            Content.RootDirectory = "Content/bin";
        }

        protected override void Initialize()
        {
            if (_fullscreen)
            {
                _width = GraphicsDevice.DisplayMode.Width;
                _height = GraphicsDevice.DisplayMode.Height;
            }

            Graphics.PreferredBackBufferWidth = _width;
            Graphics.PreferredBackBufferHeight = _height;
            Graphics.IsFullScreen = _fullscreen;

            Graphics.ApplyChanges();

            RenderTarget = new RenderTarget2D(Graphics.GraphicsDevice, _width, _height, false, SurfaceFormat.Color,
                DepthFormat.None, 0, RenderTargetUsage.DiscardContents);
            RenderTargetLights = new RenderTarget2D(Graphics.GraphicsDevice, _width, _height, false,
                SurfaceFormat.Color,
                DepthFormat.None, 0, RenderTargetUsage.DiscardContents);
            _baseRenderRectangle = GetScaleRectangle();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            SwitchGameState(_firstGameState);
        }

        private Rectangle GetScaleRectangle()
        {
            const double variance = 0.5;
            var actualAspectRatio = (float) Window.ClientBounds.Width / Window.ClientBounds.Height;

            Rectangle scaleRectangle;

            var designedResolutionAspectRatio = (float) _width / _height;

            if (actualAspectRatio <= designedResolutionAspectRatio)
            {
                var presentHeight = (int) (Window.ClientBounds.Width / designedResolutionAspectRatio + variance);
                var barHeight = (Window.ClientBounds.Height - presentHeight) / 2;

                scaleRectangle = new Rectangle(0, barHeight, Window.ClientBounds.Width, presentHeight);
            }
            else
            {
                var presentWidth = (int) (Window.ClientBounds.Height * designedResolutionAspectRatio + variance);
                var barWidth = (Window.ClientBounds.Width - presentWidth) / 2;

                scaleRectangle = new Rectangle(barWidth, 0, presentWidth, Window.ClientBounds.Height);
            }

            return scaleRectangle;
        }

        protected override void Update(GameTime gameTime)
        {
            CurrentGameState.HandleInput(gameTime);
            CurrentGameState.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            // Render the projected world into a first render target:
            GraphicsDevice.SetRenderTarget(RenderTarget);
            GraphicsDevice.Clear(Color.Black);

            var matrix = CurrentGameState.Camera.GetViewMatrix();
            SpriteBatch.Begin(transformMatrix: matrix, samplerState: SamplerState.PointClamp);
            CurrentGameState.Render(SpriteBatch);
            SpriteBatch.End();

            // Render the light layer:
            GraphicsDevice.SetRenderTarget(RenderTargetLights);
            GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 1.0f, 0);

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp);
            SpriteBatch.Draw(RenderTarget, _baseRenderRectangle, CurrentGameState.AmbientColor);
            SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, SamplerState.LinearClamp,
                transformMatrix: matrix);
            CurrentGameState.RenderLights(SpriteBatch);
            SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp);
            CurrentGameState.RenderInterface(SpriteBatch);
            SpriteBatch.End();

            // Render the result onto the screen:
            Graphics.GraphicsDevice.SetRenderTarget(null);
            Graphics.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 1.0f, 0);
            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp);
            SpriteBatch.Draw(RenderTargetLights, _baseRenderRectangle, Color.White);
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        private void SwitchGameState(GameState gameState)
        {
            if (CurrentGameState != null)
            {
                CurrentGameState.UnloadContent();
                CurrentGameState.StateSwitched -= CurrentGameStateOnStateSwitched;
            }

            CurrentGameState = gameState;
            CurrentGameState.Initialize(Content, Window, Graphics.GraphicsDevice,
                Graphics.GraphicsDevice.Viewport.Width,
                Graphics.GraphicsDevice.Viewport.Height);
            CurrentGameState.LoadContent();

            CurrentGameState.StateSwitched += CurrentGameStateOnStateSwitched;
        }

        private void CurrentGameStateOnStateSwitched(object sender, GameState newState)
        {
            SwitchGameState(newState);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace RPG.Engine.Assets
{
    public sealed class Animation : ICloneable
    {
        private readonly int _frameHeight;
        private readonly int _frames;

        private readonly List<Rectangle> _frameSourceRectangles = new();
        private readonly int _frameWidth;
        private readonly int _lifespan;

        private readonly bool _loop;

        private readonly int _startX;
        private readonly int _startY;

        private readonly int _ticksPerFrame;
        private int _age;
        private bool _paused;

        public string Name { get; }
        public string Category { get; }

        public Animation(string name, string category, int ticksPerFrame, int frames, int startX, int startY,
            int frameWidth, int frameHeight,
            bool loop)
        {
            Name = name;
            Category = category;
            _lifespan = frames * ticksPerFrame;
            _ticksPerFrame = ticksPerFrame;
            _loop = loop;
            _frames = frames;
            _startX = startX;
            _startY = startY;
            _frameWidth = frameWidth;
            _frameHeight = frameHeight;

            for (var i = 0; i < frames; i++)
            {
                _frameSourceRectangles.Add(new Rectangle(
                    frameWidth * i, startY * frameHeight, frameWidth, frameHeight
                ));
            }
        }

        public Rectangle CurrentFrame
        {
            get
            {
                var index = (_age / _ticksPerFrame + _startX) % _frameSourceRectangles.Count;
                return _frameSourceRectangles[index];
            }
        }

        public object Clone()
        {
            return new Animation(Name, Category, _ticksPerFrame, _frames, _startX, _startY, _frameWidth, _frameHeight,
                _loop);
        }

        public void Update()
        {
            if (_paused) { return; }

            _age++;

            if (_loop && _age > _lifespan) { _age = 0; }
        }

        public void Pause()
        {
            _paused = true;
        }

        public void Unpause()
        {
            _paused = false;
        }

        public void Reset()
        {
            _age = 0;
        }
    }
}
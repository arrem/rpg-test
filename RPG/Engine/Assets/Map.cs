﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;
using MonoGame.Extended.Tiled.Renderers;
using RPG.Engine.Sprites;

namespace RPG.Engine.Assets
{
    public class Map
    {
        private const string PointsLayerName = "Points";

        private Texture2D _minimapTexture;

        private readonly GraphicsDevice _graphicsDevice;

        public Map(TiledMap tiledMap, GraphicsDevice graphicsDevice)
        {
            TiledMap = tiledMap;
            Renderer = new TiledMapRenderer(graphicsDevice, TiledMap);
            _graphicsDevice = graphicsDevice;

            InitializePoints(tiledMap);
        }

        public TiledMap TiledMap { get; }
        public TiledMapRenderer Renderer { get; }

        public Dictionary<string, TiledMapObject> Points { get; } = new();

        /// <summary>
        ///     The width of this map in pixels. This property is equivalent to the <c>WidthInPixels</c> field
        ///     of the <c>TiledMap</c> behind this map.
        /// </summary>
        public int Width => TiledMap.WidthInPixels;

        /// <summary>
        ///     The height of this map in pixels. This property is equivalent to the <c>HeightInPixels</c> field
        ///     of the <c>TiledMap</c> behind this map.
        /// </summary>
        public int Height => TiledMap.HeightInPixels;

        public IEnumerable<BoxCollisionActor> CollisionActors
        {
            get
            {
                return from tile in TiledMap.TileLayers.First(e => e.Name == "Collision").Tiles
                    where !tile.IsBlank
                    select new BoxCollisionActor(tile.X * TiledMap.TileWidth, tile.Y * TiledMap.TileHeight,
                        TiledMap.TileWidth, TiledMap.TileHeight);
            }
        }

        public Texture2D MinimapTexture
        {
            get
            {
                if (_minimapTexture == null) { InitializeMinimapTexture(); }

                return _minimapTexture;
            }
        }

        private void InitializePoints(TiledMap tiledMap)
        {
            var pointsLayer = tiledMap.ObjectLayers.FirstOrDefault(l => l.Name == PointsLayerName);

            var points = pointsLayer?.Objects.ToList().FindAll(e => e.Size.IsEmpty)
                         ?? new List<TiledMapObject>();

            foreach (var point in points) { Points[point.Name] = point; }
        }

        private void InitializeMinimapTexture()
        {
            var minimapWidth = TiledMap.Width;
            var minimapHeight = TiledMap.Height;

            // Fetch all of the tiles that are in the map, giving priority to the tiles in the higher layers:
            var list = TiledMap.TileLayers[0].Tiles.Select(t => t.GlobalIdentifier).ToList();

            for (var i = 1; i < TiledMap.TileLayers.Count; i++)
            {
                for (var j = 0; j < list.Count; j++)
                {
                    if (TiledMap.TileLayers[i].Tiles[j].GlobalIdentifier != 0)
                    {
                        list[j] = TiledMap.TileLayers[i].Tiles[j].GlobalIdentifier;
                    }
                }
            }

            // Compute the average color of each distinct tile that occurs on the map:
            var distinct = list.Distinct().ToList();
            var colors = distinct.Select(id =>
            {
                var tileSet = TiledMap.Tilesets[0];
                var tileRegion = tileSet.GetTileRegion(id - 1);
                var array = new Color[tileRegion.Width * tileRegion.Height];
                tileSet.Texture.GetData(0, tileRegion, array, 0, tileRegion.Width * tileRegion.Height);

                var (r, g, b) = array.Aggregate((0, 0, 0),
                    (tuple, color) => (tuple.Item1 + color.R, tuple.Item2 + color.G, tuple.Item3 + color.B));

                return (id, new Color(r / array.Length, g / array.Length, b / array.Length));
            }).ToDictionary(e => e.id, e => e.Item2);

            // Lastly, use the computed colors to draw the map texture:
            _minimapTexture = new Texture2D(_graphicsDevice, minimapWidth, minimapHeight);
            _minimapTexture.SetData(list.Select(e => colors[e]).ToArray());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;
using Newtonsoft.Json.Linq;
using Animations = System.Collections.Generic.Dictionary<string, RPG.Engine.Assets.Animation>;

namespace RPG.Engine.Assets
{
    public sealed class AssetManager
    {
        private readonly Dictionary<string, Animations> _animationsCache = new();
        private readonly ContentManager _contentManager;
        private readonly Dictionary<string, SpriteFont> _fontCache = new();
        private readonly Dictionary<string, Map> _mapCache = new();
        private readonly Dictionary<string, Texture2D> _textureCache = new();

        public AssetManager(ContentManager contentManager)
        {
            _contentManager = contentManager;
        }

        public Texture2D GetTexture(string textureName)
        {
            if (_textureCache.ContainsKey(textureName)) { return _textureCache[textureName]; }

            var texture = _contentManager.Load<Texture2D>(textureName);

            _textureCache[textureName] = texture ??
                                         throw new ArgumentException(
                                             $"Texture with the name '{textureName} could not be found.'");

            return texture;
        }

        public Animations GetAnimations(string animationName)
        {
            if (!_animationsCache.ContainsKey(animationName)) { LoadAnimations(animationName); }

            var result = new Dictionary<string, Animation>();

            foreach (var (key, value) in _animationsCache[animationName]) { result[key] = value.Clone() as Animation; }

            return result;
        }

        private void LoadAnimations(string animationName)
        {
            var path = $"{Path.Combine(_contentManager.RootDirectory, animationName)}.json";
            var json = JObject.Parse(File.ReadAllText(path));

            var tileWidth = (int) json["tileWidth"];
            var tileHeight = (int) json["tileHeight"];
            var ticksPerFrame = (int) json["ticksPerFrame"];

            var animations = (JArray) json["animations"];

            if (animations == null)
            {
                throw new ArgumentException($"No animations array is present in animation '{animationName}'.");
            }

            var result = new Dictionary<string, Animation>();

            foreach (var a in animations)
            {
                var key = (string) a["key"];
                var category = (string) a["category"];
                
                if (key == null)
                {
                    throw new ArgumentException($"No animation key is present in animation '{animationName}'.");
                }

                result[key] = new Animation(key, category, ticksPerFrame, (int) a["frameCount"],
                    (int) a["startFrameIndex"], (int) a["framesIndex"], tileWidth, tileHeight, (bool) a["loop"]);
            }

            _animationsCache[animationName] = result;
        }

        public Map GetMap(string mapName, GraphicsDevice graphicsDevice)
        {
            if (_mapCache.ContainsKey(mapName)) { return _mapCache[mapName]; }

            var tiledMap = _contentManager.Load<TiledMap>(mapName);

            if (tiledMap == null) { throw new ArgumentException($"Map with the name '{mapName} could not be found.'"); }

            var map = new Map(tiledMap, graphicsDevice);

            _mapCache[mapName] = map;

            return map;
        }

        public SpriteFont GetFont(string fontName)
        {
            if (_fontCache.ContainsKey(fontName)) { return _fontCache[fontName]; }

            var font = _contentManager.Load<SpriteFont>(fontName);

            _fontCache[fontName] = font ??
                                   throw new ArgumentException(
                                       $"Font with the name '{fontName} could not be found.'");

            return font;
        }

        public void Unload()
        {
            foreach (var pair in _textureCache) { pair.Value.Dispose(); }
        }
    }
}
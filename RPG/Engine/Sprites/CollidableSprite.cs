﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Collisions;

namespace RPG.Engine.Sprites
{
    public abstract class CollidableSprite : Sprite, ICollisionActor
    {
        protected CollidableSprite(Texture2D texture) : base(texture) { }

        protected abstract bool PreventClipThrough { get; }

        public override Vector2 Position
        {
            get => base.Position;
            set
            {
                var delta = value - base.Position;
                base.Position = value;
                if (Bounds != null) { Bounds.Position += delta; }
            }
        }

        public virtual IShapeF Bounds { get; protected init; }

        public virtual void OnCollision(CollisionEventArgs collisionInfo)
        {
            if (PreventClipThrough) { Position -= collisionInfo.PenetrationVector; }
        }
    }
}
﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RPG.Engine.Sprites
{
    public class Light : Sprite
    {
        private readonly Color _color;
        private readonly float _opacity;

        private readonly float _radius;

        public Light(Texture2D texture, float radius, Color color, float opacity) : base(texture)
        {
            _radius = radius;
            _color = color;
            _opacity = opacity;
        }

        public override int Height => (int) (2 * _radius * Math.PI);
        public override int Width => (int) (2 * _radius * Math.PI);

        public override void Render(SpriteBatch spriteBatch)
        {
            var destinationRectangle = new Rectangle((int) (Position.X - Width / 2f), (int) (Position.Y - Width / 2f),
                Width, Height);
            spriteBatch.Draw(Texture, destinationRectangle, _color * _opacity);
        }
    }
}
﻿namespace RPG.Engine.Sprites
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}
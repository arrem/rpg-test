﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RPG.Engine.Sprites
{
    public abstract class Sprite
    {
        protected readonly Texture2D Texture;

        protected Sprite(Texture2D texture)
        {
            Texture = texture;
        }

        public virtual int Width => Texture.Width;
        public virtual int Height => Texture.Height;

        public virtual Vector2 Position { get; set; }

        public bool Destroyed { get; set; }

        public virtual void Render(SpriteBatch spriteBatch)
        {
            var destinationRectangle = new Rectangle((int) Position.X, (int) Position.Y, Width, Height);
            spriteBatch.Draw(Texture, destinationRectangle, Color.White);
        }

        public virtual void Update(GameTime gameTime) { }
    }
}
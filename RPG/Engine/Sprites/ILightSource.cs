﻿namespace RPG.Engine.Sprites
{
    public interface ILightSource
    {
        public Light Light { get; }
    }
}
﻿using MonoGame.Extended;
using MonoGame.Extended.Collisions;

namespace RPG.Engine.Sprites
{
    public class BoxCollisionActor : ICollisionActor
    {
        public BoxCollisionActor(float x, float y, float width, float height)
        {
            Bounds = new RectangleF(x, y, width, height);
        }

        public IShapeF Bounds { get; }

        public void OnCollision(CollisionEventArgs collisionInfo) { }
    }
}
﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace RPG.Engine.Interfaces
{
    public abstract class InterfaceComponent
    {
        public IShapeF Bounds { get; protected set; }
        public virtual Vector2 Position { get; set; }

        public abstract void Render(SpriteBatch spriteBatch);

        public virtual void Update(GameTime gameTime) { }
    }
}
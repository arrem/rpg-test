﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RPG.Engine.Interfaces
{
    public class Interface
    {
        private readonly List<InterfaceComponent> _components = new();

        protected void AddComponent(InterfaceComponent component)
        {
            _components.Add(component);
        }

        public void Render(SpriteBatch spriteBatch)
        {
            foreach (var component in _components) { component.Render(spriteBatch); }
        }

        public void Update(GameTime gameTime)
        {
            foreach (var component in _components) { component.Update(gameTime); }
        }
    }
}